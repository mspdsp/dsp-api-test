## API Test

This is a tool to automate the calling of all the various APIs. It will compare the previous call to the current call
and return any errors/differences encountered. 

1. The endpoint POST **/run** will kick off the whole framework.
2. This reads from the **base_config** table in the database to identify credentials and base urls for the environment we want to run against.
3. It then reads from the **test_case** table to identify the endpoints and payloads we wish to test.
4. After executing the test cases, it records the results in the **test_result** table.
5. Finally, a response is returned containing details of any cases that had errors or differences