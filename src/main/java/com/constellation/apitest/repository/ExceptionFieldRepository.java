package com.constellation.apitest.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.constellation.apitest.entity.ExceptionFields;

@Repository
public interface ExceptionFieldRepository  extends JpaRepository<ExceptionFields, Long > {
		ExceptionFields findByapiname(String apiName);
}
