package com.constellation.apitest.repository;

import com.constellation.apitest.entity.TestCase;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestUrlRepository extends JpaRepository<TestCase, Long > {
}
