package com.constellation.apitest.repository;

import com.constellation.apitest.entity.TestCase;
import com.constellation.apitest.entity.TestResult;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface TestResultRepository extends JpaRepository<TestResult, Long > {
    Optional<TestResult> findFirstByTestCaseOrderByRunTimeDesc(TestCase testCase);
}
