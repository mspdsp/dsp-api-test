package com.constellation.apitest.repository;

import com.constellation.apitest.entity.BaseConfig;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TestConfigRepository extends JpaRepository<BaseConfig, Long > {
}
