package com.constellation.apitest.util;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Iterator;

public class JsonKeyDiff {

    /**
     * Determines if two json objects have different keys.
     *
     * We check only the keys (not the values).
     * Newly added keys are ignored.
     * Order of the keys does not matter (only their presence)
     */
    public static boolean containsDifferentKeys(JsonNode from, JsonNode to) {

        Iterator<String> keys = from.fieldNames();

        while (keys.hasNext()) {
            String currentField = keys.next();
            if (!to.has(currentField)) {
                return true;
            }
            JsonNode value = from.get(currentField);
            if (value.isObject()) {
                if (JsonKeyDiff.containsDifferentKeys(value, to.get(currentField))) {
                    return true;
                }
            }
        }

        return false;
    }
}
