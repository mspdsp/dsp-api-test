package com.constellation.apitest.util;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Map;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.JsonNode;


public class JsonValueComparator {

	/**
	 * Traverses through the current and previous response
	 * to find the keys and values which are different  
	 *
	 */
	public static boolean  jsonComparator( JsonNode previousResult,  JsonNode response, String fieldsToIgnore,Map<String, String> errorLog) 
				throws JsonMappingException, JsonProcessingException {
		 Iterator<String> keys = previousResult.fieldNames();
		 Iterator<String> newKeys= response.fieldNames();
		 if(isEqual(keys,newKeys)) {
		  keys = previousResult.fieldNames();
		 while(keys.hasNext()){
			 String key = keys.next();
			 if(!key.equals("message")) {
				JsonNode value =  previousResult.get(key);
				JsonNode currentValue = response.get(key);
				if(!currentValue.equals(value)) {
					if((value.isObject() && currentValue.isObject())) {
						jsonComparator(value,currentValue,fieldsToIgnore,errorLog);
					}
					else if((value.isArray() && currentValue.isArray())) {
						if(value.size()==currentValue.size()) { 
							for(int i=0;i<value.size();i++) {
								JsonNode prevResults =	value.get(i);
								JsonNode currResults = currentValue.get(i);
								jsonComparator(prevResults,currResults,fieldsToIgnore,errorLog);
							}
						}else {
							errorLog.put("error message","Number of elements in array are different");
						}
						
					} else if (value.isTextual() && currentValue.isTextual()) {
						if(!fieldsToIgnore.contains(key)) {	// check if the key exits in the database or not
							errorLog.put(key," previous value="+value+" current value="+currentValue);
						}
					}
				}
			 }
		 } 
		 }//end if equals
		 else {
			 errorLog.put("error message","keys in two response do not match");
		 }
		 if(errorLog.size()>1) {
			 return true;
		 }
		 return false;
	}
	
	// check if number of keys is equal as well as there values are same
	 public static boolean isEqual(Iterator<String> keySet1, Iterator<String> keySet2)
	 {
		 boolean same =true; 
		 int countAkeys = 0;
		 int countBkeys = 0;
		 List<String> valuesA = new ArrayList<String>();
		 List<String> valuesB = new ArrayList<String>();
		 while(keySet1.hasNext()) {
			 valuesA.add(keySet1.next());
			 countAkeys++;
		 }
		 while(keySet2.hasNext()) {
			 valuesB.add(keySet2.next());
			 countBkeys++;
		 }
		 if(countAkeys==countBkeys) {
			 if(valuesA.equals(valuesB))
				 same = true;
		 }else
		 {
			 same = false;
		 }
		 return same;
	 }

}

			   

			       
			    
			
		

