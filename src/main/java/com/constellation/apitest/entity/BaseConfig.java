package com.constellation.apitest.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity(name = "base_config")
@Getter
@Setter
public class BaseConfig {

    @Id
    @GeneratedValue
    private Long id;

    // base url for WebAPI
    @Column(nullable = false)
    private String baseUrl;

    // base url for ContainerAPI
    @Column(nullable = false)
    private String appBaseUrl;

    // url of this app (for viewing detail reports)
    @Column(nullable = false)
    private String selfBaseUrl;

    // field that must be sent to webAPI
    @Column(nullable = false)
    private String serviceProviderId;

    // field that must be sent to webAPI
    @Column(nullable = false)
    private String apiKey;

    // field that must be sent to webAPI
    @Column(nullable = false)
    private String externalServiceName;

    // field that must be sent to webAPI
    @Column(nullable = false)
    private String externalServiceVersion;

    // field that must be sent to webAPI
    @Column(nullable = false)
    private String organizationId;

    // field that must be sent to webAPI
    @Column
    private String userKey;

    // field that must be sent to webAPI
    @Column
    private String userId;

    // field that must be sent to webAPI
    @Column
    private String userSecret;

    // field that must be sent to ContainerAPI
    @Column
    private String username;

    // field that must be sent to ContainerAPI
    @Column
    private String password;
}
