package com.constellation.apitest.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.Getter;
import lombok.Setter;

@Entity(name="Exception_fields")
@Getter
@Setter
public class ExceptionFields {

	@Id
	@GeneratedValue
	 private Long id;
	
	 @Column(nullable = false)
	    private String apiname;
	 
	 @Column(nullable = false)
	    private String fieldNames;
}
