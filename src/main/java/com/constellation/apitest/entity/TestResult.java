package com.constellation.apitest.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity(name = "test_result")
@Getter
@Setter
public class TestResult {

    @Id
    @GeneratedValue
    private Long id;

    // when the test was run
    @Column(name = "run_time", nullable = false)
    private LocalDateTime runTime;

    // JSON response we got from running the test
    @Column(name = "result")
    private String result;

    // response code we got from running the test
    @Column(name = "response_code", nullable = false)
    private String responseCode;

    @ManyToOne
    private TestCase testCase;
}
