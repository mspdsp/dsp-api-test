package com.constellation.apitest.entity;

import com.constellation.apitest.enums.ApiType;
import com.constellation.apitest.enums.TokenType;
import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.util.List;

@Entity(name = "test_case")
@Getter
@Setter
public class TestCase {

    @Id
    @GeneratedValue
    private Long id;

    // url to call
    @Column(nullable = false)
    private String url;

    // any JSON we want to send with the request
    @Column(nullable = false)
    private String input;

    // human-readable name for this test
    @Column(nullable = false)
    private String name;

    // tells the test which type of auth token we need to use
    @Column
    @Enumerated(value = EnumType.STRING)
    private TokenType tokenType;

    // which type of api we are trying to call
    @Column
    @Enumerated(value = EnumType.STRING)
    private ApiType apiType;

    @OneToMany
    private List<TestResult> results;

}
