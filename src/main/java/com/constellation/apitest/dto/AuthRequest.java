package com.constellation.apitest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class AuthRequest {
    private String userId;
    private String userKey;
    private String userSecret;

}
