package com.constellation.apitest.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class LoginRequest {
    private String serviceProviderId;
    private String apiKey;
    private String externalServiceName;
    private String externalServiceVersion;
    private String organizationId;
}
