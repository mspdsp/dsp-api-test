package com.constellation.apitest.controller;

import com.constellation.apitest.service.DetailsService;
import com.constellation.apitest.service.RunnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class DetailsController {

    @Autowired
    private DetailsService detailsService;

    /**
     * Returns full details about the differences between two runs.
     */
    @GetMapping(path = "/details")
    public String run(@RequestParam Long from, @RequestParam Long to) {
        return detailsService.getDetails(from, to);
    }
}