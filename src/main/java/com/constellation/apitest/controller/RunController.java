package com.constellation.apitest.controller;

import com.constellation.apitest.service.RunnerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
public class RunController {

    private RunnerService runnerService;

    @Autowired
    public RunController(RunnerService runnerService) {
        this.runnerService = runnerService;
    }

    /**
     * Main entry point into this tool.
     *
     * Will run through all known test cases in the database and return any whose response structure has changed since the last
     * time it was run.
     * @return
     */
    @PostMapping(path = "/run")
    public Map<String, List> run() {
        return runnerService.runAll();
    }

}
