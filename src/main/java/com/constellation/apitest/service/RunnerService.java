package com.constellation.apitest.service;

import com.constellation.apitest.dto.AuthRequest;
import com.constellation.apitest.dto.LoginRequest;
import com.constellation.apitest.dto.LoginResponse;
import com.constellation.apitest.dto.ProblemDetails;
import com.constellation.apitest.entity.BaseConfig;
import com.constellation.apitest.entity.ExceptionFields;
import com.constellation.apitest.entity.TestResult;
import com.constellation.apitest.entity.TestCase;
import com.constellation.apitest.enums.ApiType;
import com.constellation.apitest.enums.TokenType;
import com.constellation.apitest.repository.ExceptionFieldRepository;
import com.constellation.apitest.repository.TestConfigRepository;
import com.constellation.apitest.repository.TestResultRepository;
import com.constellation.apitest.repository.TestUrlRepository;
import com.constellation.apitest.util.JsonKeyDiff;
import com.constellation.apitest.util.JsonValueComparator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;

import java.time.LocalDateTime;
import java.util.*;

@Service
@Slf4j
public class RunnerService {

    private TestResultRepository testResultRepository;
    private TestConfigRepository testConfigRepository;
    private TestUrlRepository testUrlRepository;
    private RestTemplate restTemplate;
    private ExceptionFieldRepository exceptionFieldRepository;

    @Autowired
    public RunnerService(TestResultRepository testResultRepository, TestConfigRepository testConfigRepository, 
    		TestUrlRepository testUrlRepository, RestTemplate restTemplate,ExceptionFieldRepository exceptionFieldRepository) {
        this.testResultRepository = testResultRepository;
        this.testConfigRepository = testConfigRepository;
        this.testUrlRepository = testUrlRepository;
        this.restTemplate = restTemplate;
        this.exceptionFieldRepository = exceptionFieldRepository;
    }
    
    

    /**
     * Pulls test cases from the database, executes them, and returns any whose structure has changed since the last run.
     *
     * @return
     */
    public Map<String, List> runAll() {
        BaseConfig baseConfig = getTestConfig();
        List<TestCase> testCases = testUrlRepository.findAll();

        log.info("Performing authentication. This can take a while.");

        // login as a service (webAPI)
        LoginResponse loginResponse = login(baseConfig);

        // login as a user of a service (webAPI)
        LoginResponse authResponse = authorize(baseConfig, loginResponse.getData().getToken());

        // login
        LoginResponse appLoginResponse = appLogin(baseConfig);

        List<ProblemDetails> invalidBody = new ArrayList<>();
        List<ProblemDetails> invalidCode = new ArrayList<>();
        List<ProblemDetails> errorCalling = new ArrayList<>();
        String token;
        String connectorToken=null;
        for (TestCase testCase : testCases) {
            log.info("Started case: {}", testCase.getName());
            Optional<TestResult> previousResult = testResultRepository.findFirstByTestCaseOrderByRunTimeDesc(testCase);

            // choose the right auth token based on the api type
            token = selectToken(loginResponse, authResponse, appLoginResponse, testCase);
            
           //modify test case input for connector result call before executing
            if(testCase.getName().equals("connector result")) {
            	 String tokenValue = connectorToken+"\"}";
				 String input = testCase.getInput();
				 int index = input.indexOf("token")+9;
				  input = input.substring(0,index);
				  input = input.concat(tokenValue);
				 testCase.setInput(input);
            }

            // actually call the api under test
            ResponseEntity<String> response = executeTest(baseConfig, token, testCase, errorCalling);
            if (response == null) {
                continue;
            }
            
            //special case of connector token
            if(testCase.getName().equals("connector call")) {
            	connectorToken = getConnectorToken(response);
            }
            
            // get field names that will be ignored while comparing the output
            String fields = getExceptionalFields(testCase.getName());
            
            // compare the previous result with this result and persist the results
            compareCurrentAndPreviousResponses(invalidBody, invalidCode, testCase, previousResult, response, baseConfig,fields);

            log.info("Completed case: {}", testCase.getName());
        }

        // return the caller a breakdown of what happened
        Map<String, List> results = new HashMap<>();
        results.put("invalidBody", invalidBody);
        results.put("invalidCode", invalidCode);
        results.put("errorCalling", errorCalling);

        return results;
    }

    private String getConnectorToken(ResponseEntity<String> response) {
    	ObjectMapper objectMapper = new ObjectMapper();
    	String connectorToken="";
    	String body = response.getBody();
  	   try {
		connectorToken = objectMapper.readTree(body).get("data").get("token").asText();
	} catch (JsonProcessingException e) {
		log.error("Exception processing json for  connector call {}",  e.getMessage());
	}
  	   return connectorToken;
    }
    private String selectToken(LoginResponse loginResponse, LoginResponse authResponse, LoginResponse appLoginResponse, TestCase testCase) {
        String token;
        token = authResponse.getData().getToken();
        if (testCase.getTokenType() == TokenType.SERVICE) {
            token = loginResponse.getData().getToken();
        } else if (testCase.getTokenType() == TokenType.APP) {
            token = appLoginResponse.getData().getToken();
        }
        return token;
    }

    private void compareCurrentAndPreviousResponses(List<ProblemDetails> invalidBody, List<ProblemDetails> invalidCode, TestCase testCase, Optional<TestResult> previousResult, ResponseEntity<String> response, BaseConfig baseConfig, String fields) {
        ObjectMapper objectMapper = new ObjectMapper();
        Map<String, String> errors= new HashMap<String,String>();
        TestResult currentResult = persistResults(testCase, response);
        if (previousResult.isPresent()) {
            try {
        //        if (JsonKeyDiff.containsDifferentKeys(objectMapper.readTree(previousResult.get().getResult()), objectMapper.readTree(response.getBody()))) {
            	if(JsonValueComparator.jsonComparator(objectMapper.readTree(previousResult.get().getResult()), objectMapper.readTree(response.getBody()),fields, errors)) {
                    StringBuilder builder = new StringBuilder();
                    builder.append(baseConfig.getSelfBaseUrl() + "/details?from=")
                            .append(previousResult.get().getId())
                            .append("&to=")
                            .append(currentResult.getId());
                    invalidBody.add(new ProblemDetails(testCase.getName(), builder.toString()));
                }
            } catch (JsonProcessingException e) {
                log.error("Exception processing json for case {} {}", testCase.getName(), e.getMessage());
            }
            Integer currentResponseCode = response.getStatusCodeValue();
            if (!previousResult.get().getResponseCode().equals(currentResponseCode.toString())) {
                invalidCode.add(new ProblemDetails(testCase.getName(), "(Responsecode) " + previousResult.get().getResponseCode() + " => " + currentResponseCode));
            }
        }
    }

    private ResponseEntity<String> executeTest(BaseConfig baseConfig, String token, TestCase testCase, List errors) {
        ResponseEntity<String> response;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + token);
            headers.add("Content-Type", "application/json");
            headers.add("Accept", "application/json");

            HttpEntity<String> requestToExecute = new HttpEntity<>(testCase.getInput(), headers);
            String baseUrl = baseConfig.getBaseUrl();
            if (testCase.getApiType() == ApiType.CONTAINER) {
                baseUrl = baseConfig.getAppBaseUrl();
            }

            response = restTemplate.exchange(baseUrl + testCase.getUrl(), HttpMethod.POST,
                    requestToExecute, String.class);
        } catch (Exception e) {
            ProblemDetails details = new ProblemDetails(testCase.getName(), e.getMessage());
            errors.add(details);
            log.error("Exception when calling url {} to test {}", testCase.getUrl(), e.getMessage());
            return null;
        }

        return response;
    }

    private LoginResponse authorize(BaseConfig baseConfig, String platformToken) {
        ResponseEntity<LoginResponse> response;
        try {
            HttpHeaders headers = new HttpHeaders();
            headers.add("Authorization", "Bearer " + platformToken);
            AuthRequest authRequest = new AuthRequest();
            authRequest.setUserId(baseConfig.getUserId());
            authRequest.setUserSecret(baseConfig.getUserSecret());
            authRequest.setUserKey(baseConfig.getUserKey());

            ObjectMapper mapper = new ObjectMapper();
            HttpEntity<String> request = new HttpEntity<>(mapper.writeValueAsString(authRequest), headers);

            response = restTemplate.exchange(baseConfig.getBaseUrl() + "/api/v2/user/auth", HttpMethod.POST,
                    request, LoginResponse.class);
        } catch (Exception e) {
            log.error("Exception when calling url to auth {}", e.getMessage());
            return null;
        }
        if (response == null || response.getStatusCode().isError()) {
            log.error("Invalid response received calling url to auth");
            return null;
        }

        return response.getBody();
    }

    private TestResult persistResults(TestCase testCase, ResponseEntity<String> response) {
        TestResult result = new TestResult();
        result.setTestCase(testCase);
        result.setResult(response.getBody());
        result.setResponseCode(((Integer) response.getStatusCodeValue()).toString());
        result.setRunTime(LocalDateTime.now());

        testResultRepository.save(result);

        return result;
    }

    private BaseConfig getTestConfig() {
        Optional<BaseConfig> optionalBaseConfig = testConfigRepository.findById(1l);
        if (!optionalBaseConfig.isPresent()) {
            System.out.println("No base configuration set. Exiting");
            return null;
        }
        return optionalBaseConfig.get();
    }

    private LoginResponse appLogin(BaseConfig baseConfig) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);

        MultiValueMap<String, String> map= new LinkedMultiValueMap<>();
        map.add("username", baseConfig.getUsername());
        map.add("password", baseConfig.getPassword());

        HttpEntity<MultiValueMap<String, String>> request = new HttpEntity<>(map, headers);

        ResponseEntity<LoginResponse> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(baseConfig.getAppBaseUrl() + "/auth/login", request, LoginResponse.class);
        } catch (Exception e) {
            log.error("Exception when calling url to login {}", e.getMessage());
            return null;
        }
        if (responseEntity == null || responseEntity.getStatusCode().isError()) {
            log.error("Invalid response received calling url to login");
            return null;
        }

        return responseEntity.getBody();
    }

    private LoginResponse login(BaseConfig baseConfig) {
        LoginRequest loginRequest = new LoginRequest();
        loginRequest.setExternalServiceName(baseConfig.getExternalServiceName());
        loginRequest.setApiKey(baseConfig.getApiKey());
        loginRequest.setExternalServiceVersion(baseConfig.getExternalServiceVersion());
        loginRequest.setServiceProviderId(baseConfig.getServiceProviderId());
        loginRequest.setOrganizationId(baseConfig.getOrganizationId());
        ResponseEntity<LoginResponse> responseEntity;
        try {
            responseEntity = restTemplate.postForEntity(baseConfig.getBaseUrl() + "/api/v2/login", loginRequest, LoginResponse.class);
        } catch (Exception e) {
            log.error("Exception when calling url to login {}", e.getMessage());
            return null;
        }
        if (responseEntity == null || responseEntity.getStatusCode().isError()) {
            log.error("Invalid response received calling url to login");
            return null;
        }

        return responseEntity.getBody();
    }
    
    private String getExceptionalFields(String apiName) {
		  ExceptionFields fields = exceptionFieldRepository.findByapiname(apiName);
		  String fieldNames = fields.getFieldNames();
		  return fieldNames;
		  }
}
