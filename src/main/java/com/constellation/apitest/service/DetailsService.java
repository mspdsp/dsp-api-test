package com.constellation.apitest.service;

import com.constellation.apitest.entity.TestResult;
import com.constellation.apitest.repository.TestResultRepository;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.flipkart.zjsonpatch.JsonDiff;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
@Slf4j
public class DetailsService {

    private TestResultRepository testResultRepository;

    @Autowired
    public DetailsService(TestResultRepository testResultRepository) {
        this.testResultRepository = testResultRepository;
    }

    public String getDetails(Long from, Long to) {

        Optional<TestResult> fromResult = testResultRepository.findById(from);
        Optional<TestResult> toResult = testResultRepository.findById(to);

        ObjectMapper jackson = new ObjectMapper();
        JsonNode beforeNode;
        JsonNode afterNode;
        try {
            beforeNode = jackson.readTree(fromResult.get().getResult());
            afterNode = jackson.readTree(toResult.get().getResult());
        } catch (JsonProcessingException e) {
            log.error("Error parsing details json {}", e.getMessage());
            return "Unable to return json diff :( ." + e.getMessage();
        }

        JsonNode patchNode = JsonDiff.asJson(beforeNode, afterNode);
        return patchNode.toString();
    }

}