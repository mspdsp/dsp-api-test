package com.constellation.apitest.enums;

public enum TokenType {
    USER,
    SERVICE,
    APP
}
