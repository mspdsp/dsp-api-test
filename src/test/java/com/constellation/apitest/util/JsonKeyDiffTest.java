package com.constellation.apitest.util;


import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class JsonKeyDiffTest {

    @Test
    public void testNotDifferent_returnsFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\":\"Borat\"}";
        String toJson = "{\"name\":\"Borat\"}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertFalse(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testKeysSameValuesDifferent_returnsFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\":\"Borat\"}";
        String toJson = "{\"name\":\"Lebron\"}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertFalse(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testAdditionalKeysAdded_returnsFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\":\"Borat\"}";
        String toJson = "{\"name\":\"Borat\",\"color\": \"red\"}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertFalse(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testKeysSameButDifferentOrder_returnsFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\":\"Borat\", \"job\":\"actor\"}";
        String toJson = "{\"job\":\"actor\", \"name\":\"Lebron\"}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertFalse(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testBasicDifferent_returnsTrue() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\":\"Borat\"}";
        String toJson = "{\"firstName\":\"Borat\"}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertTrue(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testNestedDifferent_returnsTrue() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\": \"Borat\", \"attributes\":{\"height\":\"tall\"}}";
        String toJson = "{\"name\": \"Borat\", \"attributes\":{\"size\":\"small\"}}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertTrue(JsonKeyDiff.containsDifferentKeys(from, to));
    }

    @Test
    public void testNestedNotDifferent_returnsFalse() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String fromJson = "{\"name\": \"Borat\", \"attributes\":{\"height\":\"tall\"}}";
        String toJson = "{\"name\": \"Borat\", \"attributes\":{\"height\":\"tall\"}}";
        JsonNode from = mapper.readTree(fromJson);
        JsonNode to = mapper.readTree(toJson);
        assertFalse(JsonKeyDiff.containsDifferentKeys(from, to));
    }

}
